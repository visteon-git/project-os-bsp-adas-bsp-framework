## Gearman - Open Source Message Queuing System ##

Gearman is an open source message queuing system that makes it easy to do distributed job processing using multiple languages. With Gearman you: farm out work to other machines, dispatching function calls to machines that are better suited to do work, to do work in parallel, to load balance lots of function calls, to call functions between languages, spread CPU usage around your network.

### How do I get set up?  ###

Please refer gearman_installation_guide.docx available inside docs folder.

#### How to run the sample or test application? ####

* Run gearman job server in any one of your PC/target in your network  

	``
    $ gearmand --verbose DEBUG –L <ip addr of server machine>
    ``


* Run worker function in any one of your PC/target available in your network. *load.sh* script is available in the user-apps folder.  
  This load.sh script will be executed when client requests server to execute LOAD function

	``
    $ gearman –w -f LOAD -h <ip addr of server machine> xargs ./load.sh
    ``
    
* Initate job reqest from any PC/target available in the network  

	``
    $ gearman –f LOAD –h <ip addr of server machine> x
    ``
