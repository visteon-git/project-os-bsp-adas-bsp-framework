#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <string.h>
#include <openssl/sha.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <errno.h>
#include "ivshmem.h"

#define CHUNK_SZ (1024l*1024l*4l)
#define MSI_VECTOR 0 /* the default vector */


int do_select(int fd);

int main(int argc, char ** argv){

    long length;
    void *memptr, *regptr;
    int fd, rv;
    int other, remote_host;
    int pagesize;
    int *array;

    if (argc != 3){
        printf("USAGE: %s <filename> <other vm ID>\n", argv[0]);
        exit(-1);
    }

    fd = open(argv[1], O_RDWR);
    if (fd <= 0) {
        printf("Can't open %s\n", argv[1]);
        exit(-1);
    }
    printf("[SUM] opening file %s\n", argv[1]);
    other = atoi(argv[2]);

    length = 0;
    printf("[SUM] length is %ld\n", length);

    pagesize = getpagesize();
    if ((regptr = mmap(NULL, 256, PROT_READ|PROT_WRITE, MAP_SHARED, fd,
		       0 * pagesize)) == (void *) -1){
        printf("%d: mmap failed (0x%p)\n", __LINE__, regptr);
        perror("mmap error:");
        close(fd);
        exit(-1);
    }

    length = CHUNK_SZ;
    if ((memptr = mmap(NULL, length, PROT_READ|PROT_WRITE, MAP_SHARED, fd,
		       1 * pagesize)) == (void *) -1){
        printf("%d: mmap failed (0x%p)\n", __LINE__, memptr);
        perror("mmap error:");
        close(fd);
        exit(-1);
    }

    printf("waiting\n");
    rv = ivshmem_recv(fd, memptr);
    printf("rv = %d\n", rv);

//    printf("md is *%20s*\n", md);

    /* check for valid sender */
    array = (int *) memptr;
    remote_host = (array[Doorbell/sizeof(int)] >> 16) & 0xFFFF;
    if (other == remote_host) {
        printf("Success!! Received message is from intended (ID: %d) host!!\n", other);
    }
    else {
        printf("remote_host = %d\n", remote_host);
    }

    munmap(memptr, length);
    munmap(regptr, 256);
    close(fd);

    printf("[SUM] Exiting...\n");
}

int do_select(int fd) {

    fd_set readset;

    FD_ZERO(&readset);
    /* conn socket is in Live_vms at posn 0 */
    FD_SET(fd, &readset);

    select(fd + 1, &readset, NULL, NULL, NULL);

    return 1;

}
