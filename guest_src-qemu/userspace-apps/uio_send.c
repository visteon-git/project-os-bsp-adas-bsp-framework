#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <openssl/sha.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <sys/ioctl.h>
#include "ivshmem.h"

#define CHUNK_SZ (4*1024*1024)
#define MSI_VECTOR 0 /* the default MSI vector */
#define DATE_STR_SIZE   64
#define HOST_STR_SIZE   32


int main(int argc, char ** argv){

    int fd, length=4*1024;
    void * memptr, *regptr;
    int other;
    int pagesize;

    time_t rawtime;
    struct tm *info;
    char date_str[DATE_STR_SIZE];
    char host_str[HOST_STR_SIZE];

    if (argc != 3){
        printf("USAGE: %s <filename> <other vm ID>\n]", argv[0]);
        exit(-1);
    }

    fd = open(argv[1], O_RDWR|O_NONBLOCK);
    if (fd <= 0) {
        printf("Error in opening %s\n", argv[1]);
        exit(-1);
    }
    printf("[DUMP] opening file %s\n", argv[1]);

    other = atoi(argv[2]);

    length = CHUNK_SZ;
    printf("[DUMP] size is %d\n", length);

    pagesize = getpagesize();
    if ((regptr = mmap(NULL, 256, PROT_READ|PROT_WRITE, MAP_SHARED, fd,
		      0 * pagesize)) == (void *) -1)
    {
        printf("%d: mmap failed (0x%p)\n", __LINE__, regptr);
        perror("mmap error:");
        close(fd);
        exit(-1);
    }

    if ((memptr = mmap(NULL, length, PROT_READ|PROT_WRITE, MAP_SHARED, fd,
		       1 * pagesize)) == (void *) -1)
    {
        printf("%d: mmap failed (0x%p)\n", __LINE__, memptr);
        perror("mmap error:");
        close(fd);
        exit(-1);
    }

    time( &rawtime );
    info = localtime( &rawtime );
    strftime(date_str, DATE_STR_SIZE,"%x - %I:%M%p %S seconds", info);
    gethostname(host_str, HOST_STR_SIZE);
    sprintf((char*)memptr, "%s:%d --> This message is written to memory region pointed by %s\'s PCI BAR2 register. This is written at %s\n",
        __FILE__, __LINE__, host_str, date_str);
    printf("\nWritten to PCI BAR2 @%s\n\n", date_str);

    srand(time(NULL));

    /* wake client */
    ivshmem_send(regptr, MSI_VECTOR, other);



    /*
    printf("waiting\n");
    rv = ivshmem_recv(fd);
    printf("rv = %d\n", rv);
    */

    printf("[DUMP] munmap is unmapping %p\n", memptr);
    munmap(memptr, length);
    munmap(regptr, 256);

    close(fd);

}
